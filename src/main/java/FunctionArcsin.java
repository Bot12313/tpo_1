public class FunctionArcsin {
    public static int steps = 17;

    public static double calc(double x) {
        if (!(Math.abs(x) < 1))
            return Double.NaN;
        double res = x;
        double A = 1, B = 1;
        for(int n=1;n<steps;n++) {
            A *= 2*n - 1;
            B *= 2*n;
            res += A * Math.pow(x, 2*n+1) / (B * (2*n + 1));
        }
        return res;
    }
}
