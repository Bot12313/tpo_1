import DomainModel.*;

public class DomainMain {
    public static void main(String[] args) {
        DomainObject camera = new DomainObject("наезд камеры");
        DomainPlace place = new DomainPlace("при","съемке");
        DomainEvent event6 = new DomainEvent(camera, null, place, "", null);

        DomainObject he = new DomainObject("он");
        DomainAction guessed = new DomainAction("догадался", new String[]{"тут же"});
        DomainEvent event5 = new DomainEvent(he, guessed, "как", event6);

        DomainObject that = new DomainObject("это");
        DomainAction was = new DomainAction("был", new String[]{"всего лишь"});
        DomainEvent event4 = new DomainEvent(that, was, "но", event5);

        DomainAction start_moving = new DomainAction("начал двигаться",new String[]{"медленно", "неуклонно"});
        DomainPlace where = new DomainPlace("к", "пульту");
        DomainEvent event3 = new DomainEvent(he, start_moving, where, "когда", event4);

        DomainObject hair = new DomainObject("волосы");
        DomainPlace head = new DomainPlace("на","голове", new String[]{"бесплотной"});
        DomainAction moved = new DomainAction("зашевелились");
        DomainEvent event2 = new DomainEvent(hair, moved, head, "как", event3);

        DomainObject Arthur = new DomainObject("Артур");
        DomainAction ArthurAction = new DomainAction("почувствовал",new String[]{"неожиданно"});
        DomainEvent event1 = new DomainEvent(Arthur, ArthurAction, event2);

        System.out.println(event1.ExecuteEvent());
    }
}
