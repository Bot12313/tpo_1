import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeSort {
    public static List<int[]> mergeHistory = null;

    public static void ResetHistory() {
        if(mergeHistory == null)
            mergeHistory = new ArrayList<>();
        mergeHistory.clear();
    }

    public static void sort(int[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        System.arraycopy(a, 0, l, 0, mid);
        if (n - mid >= 0)
            System.arraycopy(a, mid, r, 0, n - mid);

        sort(l, mid);
        sort(r, n - mid);

        merge(a, l, r, mid, n - mid);

        //---

        mergeHistory.add(a);
    }

    private static void merge(int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            } else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }
}
