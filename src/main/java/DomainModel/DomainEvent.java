package DomainModel;

public class DomainEvent {
    public DomainObject getWho() {
        return who;
    }

    public DomainAction getAction() {
        return action;
    }

    public DomainPlace getWhere() {
        return where;
    }

    public DomainEvent getNext() {
        return next;
    }

    private DomainObject who;
    private DomainAction action;
    private DomainPlace where;
    private String pretext;
    private DomainEvent next;

    public String getPretext() {
        return (pretext == null ? "" : pretext + " ");
    }

    public String get() {
        return (who == null ? "" : who.get()) +
               (action == null ? "" : action.get()) +
               (where == null ? "" : where.get());
    }

    public DomainEvent(DomainObject who, DomainAction action, DomainPlace where, String pretext, DomainEvent next) {
        this.who = who;
        this.action = action;
        this.where = where;
        this.pretext = pretext;
        this.next = next;
    }

    public DomainEvent(DomainObject who, DomainAction action, String pretext, DomainEvent next) {
        this(who, action, null, pretext, next);
    }

    public DomainEvent(DomainObject who, DomainAction action, DomainEvent next) {
        this(who, action, null, null, next);
    }

    public String ExecuteEvent() {
        return getPretext() + get() + (next == null ? "" : next.ExecuteEvent());
    }
}
