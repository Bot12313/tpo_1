package DomainModel;

public class DomainObject {
    private String name;
    private String[] how;

    public String getName() {
        return name;
    }

    public String getHow() {
        return String.join(", ", how) + (how.length > 0 ? " " : "");
    }

    public String get() {
        return getHow() + getName() + " ";
    }

    public DomainObject(String name, String[] how) {
        this.name = name;
        this.how = how;
    }

    public DomainObject(String name) {
        this(name, new String[]{});
    }
}

