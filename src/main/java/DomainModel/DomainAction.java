package DomainModel;

public class DomainAction {
    private String name;
    private String[] how;

    public String getName() {
        return name;
    }

    public String getHow() {
        return String.join(", ", how) + (how.length > 0 ? " " : "");
    }

    public String get() {
        return  getHow() + getName() + " ";
    }

    public DomainAction(String name, String[] how) {
        this.name = name;
        this.how = how;
    }

    public DomainAction(String name) {
        this(name,new String[]{});
    }
}
