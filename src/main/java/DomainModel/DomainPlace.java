package DomainModel;

public class DomainPlace {
    private String pretext;
    private String name;
    private String[] how;

    public String getPretext() {
        return pretext;
    }

    public String getName() {
        return name;
    }

    public String getHow() {
        return String.join(", ", how) + (how.length > 0 ? " " : "");
    }

    public String get() {
        return getPretext() + " " + getHow() + getName() + " ";
    }

    public DomainPlace(String pretext, String name, String[] how) {
        this.pretext = pretext;
        this.name = name;
        this.how = how;
    }

    public DomainPlace(String pretext, String name) {
        this(pretext,name,new String[]{});
    }
}
