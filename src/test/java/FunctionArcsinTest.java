import javafx.util.Pair;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FunctionArcsinTest extends Assert {
    private final double D = 0.001;
    private List<Pair<Double, Double>> testValues;

    @Before
    public void before() {
        testValues = new ArrayList<>();
        for(double x=-1.5;x<=1.5;x+=0.3)
            testValues.add(new Pair<>(x, Math.asin(x)));
    }

    @Test
    public void test() {
        double fact,expect;
        for (Pair<Double, Double> testValue : testValues) {
            fact = FunctionArcsin.calc(testValue.getKey());
            expect = testValue.getValue();
            assertEquals(expect, fact, D);
        }
    }

    @After
    public void after() {
        testValues.clear();
    }
}
