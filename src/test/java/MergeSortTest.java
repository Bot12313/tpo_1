import org.junit.Assert;
import org.json.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MergeSortTest extends Assert {
    @Test
    public void test(){
        int[] key = {9, 19, 10, 17, 11, 8, 8, 15, 7, 17};

        List<int[]> values = new ArrayList<>();
        values.add(new int[]{9,19});
        values.add(new int[]{11,17});
        values.add(new int[]{10,11,17});
        values.add(new int[]{9,10,11,17,19});
        values.add(new int[]{8,8});
        values.add(new int[]{7,17});
        values.add(new int[]{7,15,17});
        values.add(new int[]{7,8,8,15,17});
        values.add(new int[]{7, 8, 8, 9, 10, 11, 15, 17, 17, 19});

        MergeSort.ResetHistory();
        MergeSort.sort(key, key.length);
        for(int i=0;i<MergeSort.mergeHistory.size();i++)
            assertArrayEquals(values.get(i), MergeSort.mergeHistory.get(i));
    }
}
