import DomainModel.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DomainModelTest extends Assert {
    DomainEvent initEvent;

    @Before
    public void Init() {
        DomainObject c_object = new DomainObject("c_object", new String[]{"a","b"});
        DomainPlace c_place = new DomainPlace("c_pre", "c_place");
        DomainEvent event2 = new DomainEvent(c_object, null, c_place, "pre", null);

        DomainObject b_object = new DomainObject("b_object", new String[]{"a","b"});
        DomainAction b_action = new DomainAction("b_action", new String[]{"b","c"});
        DomainEvent event1 = new DomainEvent(b_object, b_action, "pre", event2);

        DomainObject a_object = new DomainObject("a_object", new String[]{"a","b"});
        DomainPlace a_place = new DomainPlace("a_pre", "a_place", new String[]{"c","d"});
        DomainAction a_action = new DomainAction("a_action", new String[]{"b","c"});
        initEvent = new DomainEvent(a_object, a_action, a_place, "pre", event1);
    }

    @Test
    public void Test() {
        DomainObject who;
        DomainPlace place;
        DomainAction action;
        DomainEvent event;

        who = initEvent.getWho();
        assertEquals("a_object", who.getName());
        assertEquals("a, b ", who.getHow());

        action = initEvent.getAction();
        assertEquals("a_action", action.getName());
        assertEquals("b, c ", action.getHow());

        place = initEvent.getWhere();
        assertEquals("a_place", place.getName());
        assertEquals("c, d ", place.getHow());
        assertEquals("a_pre", place.getPretext());

        //--

        event = initEvent.getNext();
        who = event.getWho();
        assertEquals("b_object", who.getName());
        assertEquals("a, b ", who.getHow());

        action = event.getAction();
        assertEquals("b_action", action.getName());
        assertEquals("b, c ", action.getHow());

        place = event.getWhere();
        assertNull(place);

        //--

        event = event.getNext();
        who = event.getWho();
        assertEquals("c_object", who.getName());
        assertEquals("a, b ", who.getHow());

        place = event.getWhere();
        assertEquals("c_place", place.getName());
        assertEquals("", place.getHow());
        assertEquals("c_pre", place.getPretext());

        action = event.getAction();
        assertNull(action);
    }
}
